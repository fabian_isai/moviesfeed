package sigue.com.moviesfeed;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import sigue.com.moviesfeed.movies.ListAdapter;
import sigue.com.moviesfeed.movies.MoviesMVP;
import sigue.com.moviesfeed.movies.ViewModel;
import sigue.com.moviesfeed.root.App;

public class MainActivity extends AppCompatActivity implements MoviesMVP.View {

    private final String TAG = MainActivity.class.getName();
    @BindView(R.id.recycler_view_movies)
    RecyclerView recyclerViewMovies;
    @BindView(R.id.activity_root_view)
    LinearLayout activityRootView;

    @Inject
    MoviesMVP.Presenter presenter;


    private ListAdapter listAdapter;
    private List<ViewModel> resultList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        ((App) getApplication()).getComponent().inject(this);

        listAdapter = new ListAdapter(resultList);
        recyclerViewMovies.setAdapter(listAdapter);
        recyclerViewMovies.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.HORIZONTAL));
        recyclerViewMovies.setItemAnimator(new DefaultItemAnimator());
        recyclerViewMovies.setHasFixedSize(true);
        recyclerViewMovies.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.setView(this);
        presenter.loadData();

    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.rxJavaUnsuscribe();
        resultList.clear();
        listAdapter.notifyDataSetChanged();
    }

    @Override
    public void updateData(ViewModel viewModel) {
        resultList.add(viewModel);
        listAdapter.notifyItemChanged(resultList.size() -1);
        Log.d(TAG,"Informacion Nueva: "+viewModel.getName());
    }

    @Override
    public void showSnackbar(String s) {
        Snackbar.make(activityRootView, s,Snackbar.LENGTH_SHORT).show();
    }
}
