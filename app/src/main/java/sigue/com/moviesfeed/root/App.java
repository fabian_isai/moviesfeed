package sigue.com.moviesfeed.root;

import android.app.Application;

import sigue.com.moviesfeed.http.MovieExtraInfApiModule;
import sigue.com.moviesfeed.http.MovieTitleApiModule;
import sigue.com.moviesfeed.movies.MoviesModule;

public class App extends Application {

    private ApplicationComponent component;

    @Override
    public void onCreate() {
        super.onCreate();

        //si no te reconoce el DaggerApplicationComponent, es porque se nos olvido un provide en los modulos...revisar
        component = DaggerApplicationComponent.builder()
                    .applicationModule(new ApplicationModule(this))
                    .moviesModule(new MoviesModule())
                    .movieTitleApiModule(new MovieTitleApiModule())
                    .movieExtraInfApiModule(new MovieExtraInfApiModule())
                    .build();



        }

    public ApplicationComponent getComponent(){
        return component;
    }
}
