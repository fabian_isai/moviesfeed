package sigue.com.moviesfeed.root;

import javax.inject.Singleton;

import dagger.Component;
import sigue.com.moviesfeed.MainActivity;
import sigue.com.moviesfeed.http.MovieExtraInfApiModule;
import sigue.com.moviesfeed.http.MovieTitleApiModule;
import sigue.com.moviesfeed.movies.MoviesModule;

@Singleton
@Component(modules = {ApplicationModule.class,
                      MoviesModule.class,
                      MovieTitleApiModule.class,
                      MovieExtraInfApiModule.class})
public interface ApplicationComponent {
    void inject(MainActivity tarhet);
}
