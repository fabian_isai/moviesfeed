package sigue.com.moviesfeed.movies;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import sigue.com.moviesfeed.http.MoviesApiService;
import sigue.com.moviesfeed.http.MoviesExtraInfoApiService;

@Module
public class MoviesModule {

    @Provides
    public MoviesMVP.Presenter provideMoviesPresenter(MoviesMVP.Model moviesModel){
        return new MoviesPresenter(moviesModel);
    }

    @Provides
    public MoviesMVP.Model provideMoviesmodel(MoviesRepositoryInt repository){
        return new MoviesModel(repository);
    }

    @Singleton
    @Provides
    public MoviesRepositoryInt provideMoviesRepository(MoviesApiService moviesApiService, MoviesExtraInfoApiService moviesExtraInfoApiService){
        return new MoviesRepository(moviesApiService,moviesExtraInfoApiService);
    }

}
