package sigue.com.moviesfeed.movies;

import io.reactivex.Observable;
import io.reactivex.functions.BiFunction;
import sigue.com.moviesfeed.http.apimodel.Result;

public class MoviesModel implements MoviesMVP.Model {

    private MoviesRepositoryInt repository;

    public MoviesModel(MoviesRepositoryInt repository) {
        this.repository = repository;
    }

    @Override
    public Observable<ViewModel> result() {
        return Observable.zip(repository.getResultData(), repository.getCountryData(), new BiFunction<Result, String, ViewModel>() {
            @Override
            public ViewModel apply(Result result, String country) throws Exception {
                return new ViewModel(result.getTitle(), country);
            }
        });
    }
}
