package sigue.com.moviesfeed.movies;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import sigue.com.moviesfeed.R;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ListItemViewholder> {

    private List<ViewModel> list;

    public ListAdapter(List<ViewModel> list){
        this.list = list;
    }

    @NonNull
    @Override
    public ListItemViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_list_item, parent, false);
        return new ListItemViewholder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ListItemViewholder holder, int position) {
        holder.itemName.setText(list.get(position).getName());
        holder.countryName.setText(list.get(position).getCountry());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ListItemViewholder extends RecyclerView.ViewHolder{

        @BindView(R.id.text_view_title)
        public TextView itemName;
        @BindView(R.id.text_view_country)
        public TextView countryName;

        public ListItemViewholder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }
}
