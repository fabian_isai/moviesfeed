package sigue.com.moviesfeed.movies;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import sigue.com.moviesfeed.http.MoviesApiService;
import sigue.com.moviesfeed.http.MoviesExtraInfoApiService;
import sigue.com.moviesfeed.http.apimodel.OmdbApi;
import sigue.com.moviesfeed.http.apimodel.Result;
import sigue.com.moviesfeed.http.apimodel.TopMovieRated;

public class MoviesRepository implements MoviesRepositoryInt {

    private MoviesApiService moviesApiService;
    private MoviesExtraInfoApiService moviesExtraInfoApiService;

    //para la cache
    private List<String> countries;
    private List<Result> results;
    private long lastTimeStamp;

    private static final long CACHE_LIFETIME= 20 * 1000;  //cache que durara 20 seg

    public MoviesRepository(MoviesApiService moviesApiService, MoviesExtraInfoApiService moviesExtraInfoApiService){
        this.moviesApiService = moviesApiService;
        this.moviesExtraInfoApiService = moviesExtraInfoApiService;

        this.lastTimeStamp = System.currentTimeMillis();
        this.countries = new ArrayList<>();
        this.results = new ArrayList<>();
    }

    public boolean isUpdated(){
        return (System.currentTimeMillis()  - lastTimeStamp) < CACHE_LIFETIME ;
    }

    @Override
    public Observable<Result> getResultFromNetwork() {

        Observable<TopMovieRated> topMovieRatedObservable = moviesApiService.getTopMoviesRated(1)
                .concatWith(moviesApiService.getTopMoviesRated(2))
                .concatWith(moviesApiService.getTopMoviesRated(3));

        return topMovieRatedObservable.concatMap(new Function<TopMovieRated, Observable<Result>>() {
            @Override
            public Observable<Result> apply(TopMovieRated topMovieRated){
                return Observable.fromIterable(topMovieRated.getResults());
            }
        }).doOnNext(new Consumer<Result>() {
            @Override
            public void accept(Result result) {
                results.add(result);
            }
        });

    }

    @Override
    public Observable<Result> getResultFromCache() {
        if (isUpdated()) {
            return Observable.fromIterable(results);
        } else{
            lastTimeStamp = System.currentTimeMillis();
            results.clear();
            return Observable.empty();
        }
    }

    @Override
    public Observable<Result> getResultData() {
        return getResultFromCache().switchIfEmpty(getResultFromNetwork());
    }

    @Override
    public Observable<String> getCountryFromNetwork() {

        return getResultFromNetwork().concatMap(new Function<Result, Observable<OmdbApi>>() {
            @Override
            public Observable<OmdbApi> apply(Result result) {
                return moviesExtraInfoApiService.getTopMoviesRated(result.getTitle());
            }
        }).concatMap(new Function<OmdbApi, Observable<String>>() {
            @Override
            public Observable<String> apply(OmdbApi omdbApi) {
                return Observable.just(omdbApi.getCountry());
            }
        }).doOnNext(new Consumer<String>() {
            @Override
            public void accept(String country) throws Exception {
                countries.add(country);
            }
        });


    }

    @Override
    public Observable<String> getCountryFromCache() {
        if (isUpdated()) {
            return Observable.fromIterable(countries);
        } else{
            lastTimeStamp = System.currentTimeMillis();
            countries.clear();
            return Observable.empty();
        }
    }

    @Override
    public Observable<String> getCountryData() {
        return getCountryFromCache().switchIfEmpty(getCountryFromNetwork());
    }
}
