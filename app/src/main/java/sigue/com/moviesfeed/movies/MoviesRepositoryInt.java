package sigue.com.moviesfeed.movies;

import io.reactivex.Observable;
import sigue.com.moviesfeed.http.apimodel.Result;

public interface MoviesRepositoryInt {

    Observable<Result> getResultFromNetwork();
    Observable<Result> getResultFromCache();
    Observable<Result> getResultData();

    Observable<String> getCountryFromNetwork();
    Observable<String> getCountryFromCache();
    Observable<String> getCountryData();


}
