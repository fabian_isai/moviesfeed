package sigue.com.moviesfeed.http;



import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;
import sigue.com.moviesfeed.http.apimodel.TopMovieRated;

public interface MoviesApiService {

    @GET("top_rated")
    Observable<TopMovieRated> getTopMoviesRated( @Query("page") Integer page);
    //sin el parametro del api_key en el ejemplo...se puso en el interceptor
    //@Query("api_key") String api_key,
}
