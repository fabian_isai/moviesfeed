package sigue.com.moviesfeed.http;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;
import sigue.com.moviesfeed.http.apimodel.OmdbApi;
import sigue.com.moviesfeed.http.apimodel.TopMovieRated;

public interface MoviesExtraInfoApiService {

    @GET("/")
    Observable<OmdbApi> getTopMoviesRated(@Query("t") String title);

}
